---
author: Cameron Heard
title: My Digital Garden
date: 2024-09-03
description: The landing page for my Digital Garden
category: Garden
draft: false

---

[What is a Digital Garden?](https://maggieappleton.com/garden-history)

---


* Ham Radio - My favorite and 2nd longest running hobby.
    * [Ham Radio Crash Course Youtube Channel](https://www.youtube.com/@HamRadioCrashCourse)
    * [Archived manuals, books, publications](https://archive.org/details/dlarc)
    * [HamStudy](https://hamstudy.org)
    * [Amateur Radio Digital Communications](https://www.ardc.net/)
* Music - All of the favorite music that comes to my mind
    * [Music](/garden/music/)
    * [Hi-Fi]() *coming soon*
        * [How-To / DIY]() *coming soon*
        * [Gear](/garden/mygear/)

* Information Technology - Hey look I turned my longest running hobby into a job. 
    * How to Fake being a Sysadmin - This section assumes you know a weeeeee bit about it though.
        * [Add or Remove users from Distribution Groups within Exchange Admin panel](/garden/distlistaddrem/)
        * [Use ChatGPT to parse all of your manuals so you can ask contextual questions and get viable answers]() *coming soon*
