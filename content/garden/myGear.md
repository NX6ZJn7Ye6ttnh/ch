---
author: Cameron Heard
title: My listening gear
date: 2024-09-07
description: The ever growing list of crap I use to listen to music
category: Garden
draft: false
---

I'm going to categorize this into sub lists like headphones, DAC's, Amps, etc. 

## Headphones
    - Hifiman Sundara
    - Harmonicdyne Eris (Zeos Collab)
    - Fiio Jade JT1

---

## IEM's
    - KZ ZS10
    - INAWAKEN DAWN Ms

---

## Wireless Earbuds
    - Soundcore Liberty 4
    - Soundcore Space A40

---

## Amplifiers - Portable
    - FiiO Q11 - Also a DAC

---

## Amplifiers - desktop
    - Mayflower Electronics O2
    - Fosi Audio V3 - This is a Class D speaker amplifier, but it works GREAT as a headphone amplifier.
    - [DIY Class A headphone amplifier]() *coming soon*

---

## Streamers
    - Bluesound Node 2

---



*to-do* - Generate a ToC for easier nav. Not a priority right now 