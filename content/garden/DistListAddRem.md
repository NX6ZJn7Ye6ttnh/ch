---
author: Cameron Heard
title: My Digital Garden > Information Technology > Add or Remove users from Distribution Groups within Exchange Admin panel
date: 2024-09-03
description: Add or Remove users from Distribution Groups within Exchange Admin panel
category: Garden
draft: false
---

# Using your administrator / Service account:

* admin.microsoft.com 
    * Exchange
        * Groups
            * Distribution List 
                * (Name of group) 
                    * Members 
                        * View all and manage members
                        * Add or Remove the desired user

