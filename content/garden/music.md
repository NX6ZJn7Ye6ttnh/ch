---
title: Music section of my Digital Garden
category: Garden
---

None of this is in any particular order as I just put them in here as I think about them. 

[Stop Streeaming Music and buy the albums](/posts/stopstreaming/) 

"You should seek what you enjoy listening to, not what someone tells you to enjoy."

---

* ## [Deftones](https://www.deftones.com/)
You know them, I know them, he she they know them. Albums previous to Ohms are really compressed if they are ripped from a CD. You have to find original Vinyl presses or second hand master copies to really find the airy goodness this band deserves. A victim of the industry, really. They were pulled into the [Loudness War](https://en.wikipedia.org/wiki/Loudness_war)
    
    Notable tracks: 
    - Genesis
    - Swerve City
    - Root
    - Passenger

---

* ## [Devin Townsend (Project)](https://hevydevy.com/)
Long time Metal Head with grrrreat hair, He goes back to the mid 80's. Amazing vocal range from operatic belts to absolutely disgusting screams, their mastering technique for most albums is great Quite a bit of room in the mix for everything to breathe and not be worried about clipping.
    
    Notable Tracks
    - Deadhead
    - Kingdom
    - Supercrush

---

* ## [Pelican](https://www.pelicansong.com/)
Instrumental, doomy, sludgey, great for ```Studying / relaxing to (tm)```
    
    Notable Tracks
    - Strung up From The Sky


---

* ## [The Contortionist](https://www.thecontortionist.net/)
Progressive Metal band with heavily melodic vocals. Each band member brings immense talent to the table. One thing I don't like about the band isn't their performances, but how their albums are mastered. Like most these days, they compress the sum mix and leave it lacking drastically in dynamic range, which is sadening for me, because they "came-of-age" well into and on the down side of the Loudness War in Metal. 
    
    Notable Tracks
    - Primordial Sound
    - Language I: Intuition
    - Clairvoyant

---

* ## [Gunship](https://www.gunshipmusic.com/)
Synthwave rock band that caught me by storm. The lack of dynamic range fits this band's artistic style as it's all electronic aside from the guitars, and saxaphone. 
Oh yeah you read that right, they have a saxophone. 
    
    Notable Tracks
    - Monster In Paradise

---

* ## [✝✝✝ Crosses](https://www.crossesmusic.com/)
One of the many side projects of the honorable Chino Moreno from Deftones. Hard rock with a heavy infulence of synth heavy post-modern and electronica. It's like spooky R&B imo. 
    
    Notable Tracks
    - Light As A Feather
    - Bitches Brew
    - Sensation

---

* ## [TooL](https://www.toolband.com/)
Touted as "Radiohead for Juggalos", They bring polyrythmic time signatures, raw emotion, and plain weirdness into their music with tribal drums and unexpected guitar effects. 
    
    Notable Tracks
    - Vicarious
    - Schism
    - Fear Inoculum

---

* ## [Alcest](http://alcest-music.com/)
I'd consider this band a ambient vocal doomy shoegaze. A Duo from France using their vocal techniques in a more instrumental manner than what you would consider normal. Really great to listen to during heavy rain and staring outside. This is one of the few acts that I consider to be okay with squashed dynamic range as it helps bring up the vocals in the mix, and matches the `aesthetic` of the band. 


    Notable tracks
    - Sapphire
    - Kodama
    - L'Envol


---

* ## [Mirrored Theory](https://soundcloud.com/mirrored-theory)
Electronic music artist from the UK. Very ambient, and drum focused. I've been listening to this fella since the early 2010's because his music was the intro / outro music of a Comedy Youtuber I watched back then. Their music is quite calming to me, and brings up an emotion of meloncholy filled catharsis. Like "I know something bad happened but it was what was supposed to happen." 

    Notable Tracks
    - Space Ocean
    - The Great View
    - Orb


---

**Newer Artists That I'm getting down with**

- [SARM](https://soundcloud.com/sarrm)
- [Amira Elfeky](https://www.amiraelfeky.com/)
- [SWEET//SPINE](https://sweetspine.rocks/)
- [Not Enough Space](https://notenoughspacecom.wordpress.com/about/) -Looks like they abandoned the website in favor of a Linktree. 
- [CHVRCHES](https://chvrch.es/)
- [Terminal Sleep](https://terminalsleep.bandcamp.com/)

---

