---
author: Cameron Heard
title: Stop Streaming music - Buy the album instead
date: 2024-09-06
description: Stop Streaming music - Buy the album instead
draft: false
---

I am surely guilty in this camp as well. I love streaming music. But I've realized that I'm consuming music much like I watch TikTok videos...  *mindlessly...*
Which doesn't do me any good in *enjoying* music anymore. I'm moving towards a "Self Hosted Spoitfy". Yep, that's right. I'm going back to the iPod. 

```
╔═══╗ ♪
║███║ ♫
║(●)║ ♫
╚═══╝♪♪
```

Let me elaborate on mindlessly consuming music. 
I've noticed that unless I'm actively listening to music, I don't actually enjoy it. I use it as brain rotting white noise. 
That doens't really flow with how I respect music as a whole. I almost see music as a spiritual experience. With these sounds and words, someone is telling me a story of an 
event in time, important enough for them to take time and extrapolate their feelings into words, and paint it onto an auditory canvas for others to hear. It would be a 
blantant disservice, and lack of respect to the artist to just consume it as background noise.

Buying albums better supports the artists. 
This isn't hyperbole, this is absolute fact, especially if they are independent artists without massive label contracts. Streaming, on average, a single stream equates 
to about .3 cents. So it takes three streams to make a single penny. If you were to buy directly from the artist, and pay $10 to download the album, that equates 
to roughly 3,500 streams. Streaming is more convenient, but actually buying it ensures those artists can keep making music. That's pretty cool in my opinion. 

You buy it, you keep it. 
If you buy the album, be it a CD, LP, or digital download; as long as you keep the files / discs... That music is yours, forever. You are able to format shift them 
into whatever you'd like. So you can buy an entire CD discography from an artist, or heck even on eBay if they are out of press, you can rip those CD's and have 
the audio files on your computer to put on your iPod, or your own personal media server. (This will become a post in its own; how to create your own music cloud). 

It's a way to disconnect from the internet world and experience a great story from someone you want to hear from. It's like receiving a letter, reading it intentfully, 
and responding with your own letter. In this case you're not sending them a letter back, but you are taking time out of your limited life to appreciate what they are saying. 
There's no better way to respect that than buying and listening to it intently. 

---

## Update 2024/10/21: 

Finally received the iPod last week. Been jamming on it almost exclusively. It's so nice being disconnected while listening to music. No notifications, no 
easy way to get distracted like with the phone, the only thing I'm thinking about when using it is what I'm going to listen to next. 

Pro Tip for people that buy the iPod Classic with the iflash chip from eBay: Use the development build of Rockbox to get it to work correctly. 