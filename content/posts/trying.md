---
author: Cameron Heard
title: I'm really trying
date: 2024-18-09
description: It's hard to meet expectations on both fronts of life. I get it. 
draft: true
---

It's really hard to feel like you are doing a good job at life when the _one_ person you are seeking validation from consistently wants you to change, do more, or makes you feel like you are consistently not operating at their standards. 
It's even harder when they expect you to meet *them* where they are at all times while they are not willing to meet *you* where you are. 
--
Do you have the opportunity to get a good, well paying job with little overhead so you can be a family man? for awhile it's enough, but then their expectations increase, they want more money in the bank account, an abundance of life's offerings instead of being happy with what you have, and then a bit more. So you trick yourself into hating your current job because that's the only way you can bring yourself to leave it. You get an even higher paying job, that comes with tradeoffs in the area work-life balance though. Very soon after you change jobs to get more money, to provide that abundance, you are being chastised for not as present because your job now exhausts your mind and body from working harder to get the quality of life they wanted you to provide, for not having time to get the home to-do list done, it's harder to sit on the floor and play with your kid for hours on end. But hey, they have all they ever wanted, you changed for them, you provide all they ask and beyond. Why isn't that enough for them? You think to yourself, "I'm just not doing my job well enough, I better consume myself with it to where I do *SO GOOD* in my work that I **CAN** be home more often, get the to-do list done, get the lawn mowed twice a week."

That didn't work... Then the back and forth disagreements start, leading to multiple weekly fights, blow outs, resentment increases towards one-another. All you did was PRECICELY what they wanted of you in the first place. it's nearly impossible to make 60% more money, and be at home *more* than you were before. Obligations, work responsibilities change with taking that pay raise, you wonder why that's  Then you start to disconnect from the reality of home, checking out as soon as you walk in the door to your kitchen every day; only after you realize that the only place that has shown you respect, and appreciation for your dedication, -to what you originally thought was your family- is your job. So you further dive into it to find at least some sort of self worth, and purpose because obviously you're not good at being a spouse or parent.

Take the above, add internalizing / masking your Persistent Depressive Disorder, AuDHD (Contraction of ADHD and ASD), and severe anxeity as to not upset your spouse for being a burden on them. 

It's no wonder you are exhausted.