---
author: Cameron Heard
title: Services
date: 2025-01-23
description: Services that I provide to all internet users. 
draft: false
---

I tend to self-host services. 
There are some I find a public benefit. 
Here is a list. 

---


- [SearXNG](https://search.cameronheard.com)
    - Free Open Source search engine implementation that has an extreme focus on privacy and usability. [Check it out on Github!](https://github.com/searxng/searxng)




## Disclaimer I think?
I'm just exposing these things to the internet. All logs relating to network connections and metadata are shredded to /dev/null/. Boxes are rebooted once a day. Only local user / system logs are kept. So don't expect 100% uptime.  