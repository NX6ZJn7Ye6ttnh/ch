---
author: Cameron Heard
title: Winter Is Coming
date: 2024-01-11
description: Where does the time go?
draft: false
---


# Winter 
is yet again right around the corner. I've been telling myself for years that I need to move down South to avoid the seasonal affective disorder that rears it's ugly head
every year. I have a feeling it will be a bit less impactful this year as I'll be quite busy with life things, and I now have my own little hidey hole in the basement that 
is a sliver of my own space. I still have some cleanup and organization to do in there but I keep chipping away at it each night after I get the little to bed. 
I'm also going to try and not eat like utter crap this winter to help keep the spirits up. Plenty of warm, comforting foods that make me feel happy. 

I'm what most people would
call a foodie... But it's for very specific scenarios or times of the year. I'll be cooking a lot of roasts, making many soups, potatoes on deck all season long. Hearty, 
filling, savory dishes are what come to mind when I think of a "happy" winter menu. 
I think with winter coming I will get my Ham Radio antenna back up in the one remaining tree we have in the back yard. That way I can have my radio in the hidey hole and run some digital radio operations when holed up to escape the cabin fever. It's either going to be really mild this winter, or it's going to be one hell of a snow dropper. Either way it is going to be colder than I like so I'm going to be spending most of my time inside. 