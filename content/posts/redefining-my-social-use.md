---
author: Cameron Heard
title: Redefining my Social Media usage
date: 2024-08-30
description: redefining how I decide to consume / utilize Social Media
draft: false
---

I have bounced between social plaforms for a long time; how I use them, and what I expect out of them, yet always reverting to the original way that the platform intended me to use it. I'm going to look at it in a different frame going forward; What do I want out of my life, and how can this help me accomplish it? I want to set certain goals, come back to this document to reference them every now and again so I don't lose perspective. So this is more of a goal document to help me more than anything.

---

# What goals do I want to accomplish in my life?
1. Become less reliant on centralized technologies. 
    - This is due to my ever growing fear of something catostrophic happening in my life time and having my entire "toolbox" vanish before my eyes. 
2. Learn how to become more self sufficent `AFK`. 
    - I want to learn how to grow food, raise animals, gather clean water, generate electricity to operate my digital decentralized toolbox. 
3. Be more present. Here and now. 
    - Life is ever fleeting, and at an increasing perceived rate these days. I want to take more time, live a bit more slowly.

---

# How will Social Media help me accomplish these goals?
- In moderation, I think most Social sites, like Youtube and TikTok (time limited) would help me in goal 2. 
- Using video sharing sites like Youtube, discussion forums, and publications would help me with what I need in goal 2. 
- Utilizing this website and other decentralized technologies (self hosting for example) will aid in Goal 1 via practical on-hands learning. 
- Reducing the ***crap*** that I surround myself with digitally will help with goal 3. After removing digital crap, physical crap follows. That's another topic entirely... Simplicity vs minimalism, I hope to write about that some day. 

---

# What social sites are absolute trash? 
- Facebook
    - No meaningful interactions in most cases. Really it's just a group calendar with fluff that keeps you on it. - stopped using
- X
    - Even if I unfollow all of the political and depressing accounts, and try to steer away from all of that garbage to re-train the algo... They seem to make their way back into my field of view. - stopped using
- Reddit
    - I don't have an account on it anymore. And the only way I use it is by googling terms with "reddit.com" appended to the end of the search.

---

# What social sites / platforms are beneficial to me?
- Discord
    - Allows communication with most of my friends as most of us don't use Facebook in a meaningful capacity. 
- Mastodon
    - It's decentralized, and I can follow who I like, without ads or promoted posts.
- Good Ol' SMS
    - Have number, will text.
- Youtube
    - It's a necessary evil at this point in time. I'm trying to suppliment as much as I can with Odysee. But for the time being, Youtube is where I consume most of the content relating to what ever topic I'm looking into. 

---

# What are habits I need to change to fulfill the above goals?
- Stop taking in brain rot, and consume more how-to / idea generating content. 
- Read more to stave off the need for doom scrolling. 
- Dedicate Social time to certain devices or places. 

---

When this is all said and done, I'm hoping I'm a more productive human in the grand scheme of things. lessening my reliance on grocery stores, companies, and other people for things my family and I need to survive. That ties into being more present for my family. Better equipped to reason with, and raise productive members of soceity. My social tendencies directly reflect how they interpret device usage.