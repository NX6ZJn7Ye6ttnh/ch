:star: Hello and welcome! This is my digital "front yard". :star:

Pull up a chair, sit between the trees for some shade, and relax. Grab a couple Elderberries as well if you'd like. 

---

On your way out, take a look at my favorite Pixel Art space-y wallpaper. [Neboooola](https://cameronheard.com/img/Space%20Background1-1.png)

---

* [The Small Web is Beautiful](https://benhoyt.com/writings/the-small-web-is-beautiful/)
* [The IndieWeb is the new and the old web](https://www.alexhyett.com/newsletter/the-indie-web-is-the-new-and-the-old-web/)
* [IndieWeb: A people-focused alternative to the “corporate web” ](https://news.ycombinator.com/item?id=26950009) -Look at those comments

---